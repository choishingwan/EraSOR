# Introduction
In this page, we included some useful scripts that are relevant to EraSOR processing

## Scripts for performing all analyses in our paper
You can find all the scripts used for the analyses performed in our paper [here](https://gitlab.com/choishingwan/sample_overlap_paper.git). 

We also included a singularity image containing most of the required software for the analyses. You can download the image using the following command, assuming that you have [singularity installed](https://sylabs.io/guides/3.6/user-guide/quick_start.html#quick-installation-steps):


```
singularity pull library://choishingwan/erasor/erasor:latest
```

## Calculating LD score
To perform EraSOR adjustment, you will need to calculate the LD score. While you can download pre-computed LD scores following links [here](https://github.com/bulik/ldsc#where-can-i-get-ld-scores), you might also want to compute your own LD scores. 

You will need the following

1. A set of genotype file as a LD reference. Can use 1000 genome. 
2. List of SNPs included in your target genotype file. This is to calculate the LD score weight. 
3. [LD score](https://github.com/bulik/ldsc)
    - If you have singularity installed on your machine, and you do not want to install LD score on your own, you can use the image we have constructed. (see the above section)
4. Download the baseline annotations (here)[https://storage.googleapis.com/broad-alkesgroup-public/LDSCORE/baselineLD_v2.2_bedfiles.tgz]. Those should in theory improve the LD score estimation
5. [plink1.9](https://www.cog-genomics.org/plink/1.9/)
6. [Annotator](https://gitlab.com/choishingwan/ldsc_annotator) to generate the annotation file required by LD score. We also have a [singularity container](https://cloud.sylabs.io/library/choishingwan/prset_analyses/prset_analyses_container) with this software installed.
7. R installed, with the following packages
    - `data.table`
    - You can install `data.table` with `install.packages("data.table")` within an R session
!!! Note
    You don't necessarily need `R` or `data.table`. This is just how I achieve this tasks. You can use other methods if you like

The most efficient way of calculating the LD score is via the following procedure:

1. Split your genotype file into each chromosome
    - Maybe down sample your genotype file if you have large sample size

    ```bash
    for i in `seq 1 22`; 
    do
        ./plink \
            --bfile <genotype prefix> \
            --chr ${i} \
            --out chr${i} \
            --make-bed
    done
    ```

2. Convert the baseline annotations to the format required by LD score
    - This step can be challenging. We have implemented an annotator to do this work, but you are welcome to use other tools

    ```bash
    beds=""
    # We collect all bed annotation files into a comma separated list of files
    # e.g. beds=first_annot.bed,second_annot.bed
    # so that the scripts become annotator --target chr${i} --out baseline-${i} --bed first_annot.bed,second_annot.bed

    files=`ls *bed | grep -v chr${i}.bed`
    function join_by { local IFS="$1"; shift; echo "$*"; }
    beds=`join_by , ${files}`
    for i in `seq 1 22`;
    do
        # Use ./annotator if annotator is not installed in your path
        # or use ./<name of annotator> if you have compiled annotator to another name
        annotator \
            --target chr${i} \
            --out baseline-${i} \
            --bed ${beds}
    done
    # This script will generate baseline-#.snp files which can be used for LD score calculation
    ```

3. Modify the annotations to better fit LD scores expectation
    - Here we will use `R`, with package `data.table` . 
    ```R
    library(data.table)
    for( chr in 1:22){
        annotate <- fread(paste0("baseline-", chr))[, -c("Background")]
        # Renaming the columns
        setnames(annotate, "Base", "baseline")
        fwrite(annotate, paste0("baseline-",chr,".annot"), sep="\t")
        # For weight, we remove MHC as suggested by LDSC (assign 0 to the membership)
        annotate <- annotate[, c("CHR", "BP", "SNP", "CM", "baseline")][
            (CHR==6 & BP >= 25000000 & BP <= 34000000), baseline := 0]
        fwrite(annotate, "weight-",chr,".annot", sep="\t")
    }
    ```

4. Calculate the baseline score using the baseline annotations

    ```bash
    for i in `seq 1 22`; 
    do
        # replace ldsc.py with python ldsc.py if LDSC was not installed to your path location
        ldsc.py \
                --l2 \
                --bfile chr${i} \
                --ld-wind-kb 1000 \
                --annot baseline-${i}.annot \
                --out baseline-${chr}
    done
    ```

    !!! Warning
        ldsc require python version 2 instead of 3. You can use `pyenv` to control your python version. `pyenv` can be found [here](https://github.com/pyenv/pyenv)

5. Calculate the LD score weight

    ```bash
    for i in `seq 1 22`; 
    do
        ldsc.py \
            --l2 \
            --bfile chr${i} \
            --ld-wind-kb 1000 \
            --annot weight-${i}.annot \
            --out weight-${chr}
    done
    ```

!!! Note
    If you have the cM information, you can use `--ld-wind-cm` instead.

