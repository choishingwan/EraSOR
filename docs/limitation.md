# Introduction
Here are some limitations to EraSOR

1. As EraSOR relies on LD score intercept estimates for adjustment, all assumptions of LDSC also apply to EraSOR
    - Level of genetic and environmental stratification assumed to be the same in two cohorts
        - If GWAS is trans-ethnic, but target genotype is European, EraSOR will likely over-adjust
        - LD panel should also be representative of the base and target data

        !!! Important
            EraSOR should only be applied to single-ancestry base and target data sets that are closely matched by ancestry to each other

    - Require sufficient sample size for both the base GWAS and target
        - Minimum of 1,000 samples, but require > 5,000 for more accurate results

2. EraSOR tends to over-correct

    !!! Note
        LeBlanc et al.’s equations were based on the null model of no contribution of SNP to the trait. 
        
        There will be upper-bound to the level of inflation caused by sample overlap when there are signals.

        If we can account for this upper-bound, we might be able to "solve" the over-correction of EraSOR. Feel free to email us if you are interested or if you have an idea how to estimate this.


3. Might fail to work if base and target are of equal size and are completely overlap. 
    - This might not be a suitable data for PRS analyses



