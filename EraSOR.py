#!/usr/bin/env python3
from __future__ import print_function

from numpy.linalg.linalg import cholesky

"""
(c) 2018-2023 Shing Wan Choi

Erase Sample Overlap and Relatedness (EraSOR) is a python script for removing bias introduced by inter-cohort sample overlaps and relatedness. 
This is particularly useful for Polygenic Risk Score analyses where the GWAS summary statistics should be independent from the genotype type data use for constructing the polygenic risk score. 

"""
import numpy as np
from numpy.linalg import inv
import pandas as pd
import argparse
import functools
import gzip
import bz2
import sys
import itertools as it
import traceback
from scipy.stats import chi2, norm
from scipy import linalg
import copy
import os
from ldsc import parse as ps
from ldsc import regressions as reg
import math

np.seterr(invalid="ignore")

try:
    version_check = pd.DataFrame({"A": [1, 2, 3]})
    version_check.sort_values(by="A")
except AttributeError:
    raise ImportError("EraSOR requires pandas version >= 0.17.0 (as required by LDSC)")


__version__ = "1.1.0"
programName = "EraSOR"
MASTHEAD = "*********************************************************************\n"
MASTHEAD += "* Erase Sample Overlap and Relatedness ({N})\n".format(N=programName)
MASTHEAD += "* Version {V}\n".format(V=__version__)
MASTHEAD += "* (C) 2018-2021 Shing Wan Choi\n"
MASTHEAD += "* Icahn School of Medicine at Mount Sinai\n"
MASTHEAD += "* GNU General Public License v3\n"
MASTHEAD += "*********************************************************************\n"
pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)
pd.set_option("precision", 4)
pd.set_option("max_colwidth", 1000)
np.set_printoptions(linewidth=1000)
np.set_printoptions(precision=4)

_N_CHR = 22
COMPLEMENT = {"A": "T", "T": "A", "C": "G", "G": "C"}
# bases
BASES = COMPLEMENT.keys()
# Only keep SNPs that are not ambiguous
VALID_SNPS = {
    x
    for x in map(lambda y: "".join(y), it.product(BASES, BASES))
    if x[0] != x[1] and not x[0] == COMPLEMENT[x[1]]
}
MATCH_ALLELES = {
    x
    for x in map(lambda y: "".join(y), it.product(VALID_SNPS, VALID_SNPS))
    # strand and ref match
    if ((x[0] == x[2]) and (x[1] == x[3])) or
    # ref match, strand flip
    ((x[0] == COMPLEMENT[x[2]]) and (x[1] == COMPLEMENT[x[3]])) or
    # ref flip, strand match
    ((x[0] == x[3]) and (x[1] == x[2]))
    or ((x[0] == COMPLEMENT[x[3]]) and (x[1] == COMPLEMENT[x[2]]))
}  # strand and ref flip
# T iff SNP 1 has the same alleles as SNP 2 w/ ref allele flip.
FLIP_ALLELES = {
    "".join(x): ((x[0] == x[3]) and (x[1] == x[2])) or  # strand match
    # strand flip
    ((x[0] == COMPLEMENT[x[3]]) and (x[1] == COMPLEMENT[x[2]]))
    for x in MATCH_ALLELES
}
parser = argparse.ArgumentParser()
# Outputs
parser.add_argument(
    "--out",
    default = programName,
    type = str,
    help = "Output filename prefix. If --out is not set, "
    "{N} will use {N} as the default output filename "
    "prefix.".format(N=programName),
)
# File inputs
parser.add_argument(
    "--base",
    default = None,
    type = str,
    required = True,
    help = "Summary statistic file generated from the base GWAS",
)
parser.add_argument(
    "--target",
    default = None,
    type = str,
    required = True,
    help = "Summary statistic file generated using the target samples.",
)

# Sample information (Numbers)
samples = [ "base", "target" ]
for i in samples:
    parser.add_argument(
        "--"+i+"-N",
        default=None,
        type=float,
        help="Sample size of {N}. If this option is not set, will try to infer the sample "
        "size from the input file. If the input file contains a sample size "
        "column, and this flag is set, the argument to this flag has priority.".format(N=i),
    )
    parser.add_argument(
        "--"+i+"-N-cas",
        default=None,
        type=float,
        help="Number of cases in {N}. If this option is not set, will try to infer the number "
        "of cases from the input file. If the input file contains a number of cases "
        "column, and this flag is set, the argument to this flag has priority.".format(N=i),
    )
    parser.add_argument(
        "--"+i+"-N-con",
        default=None,
        type=float,
        help="Number of controls in {N}. If this option is not set, will try to infer the number "
        "of controls from the input file. If the input file contains a number of controls "
        "column, and this flag is set, the argument to this flag has priority.".format(N=i),
    )
    # Column of input file
    parser.add_argument(
        "--"+i+"-snp",
        default=None,
        type=str,
        help="Name of SNP column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-N-col",
        default=None,
        type=str,
        help="Name of N column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-N-cas-col",
        default=None,
        type=str,
        help="Name of column containing information of number of cases in {M} "
        "(if not a name that {N} understands). NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-N-con-col",
        default=None,
        type=str,
        help="Name of column containing information of number of controls in {M}"
        " (if not a name that {N} understands). NB: case insensitive.".format(
            N=programName, M=i
        ),
    )
    parser.add_argument(
        "--"+i+"-a1",
        default=None,
        type=str,
        help="Name of A1 column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-a2",
        default=None,
        type=str,
        help="Name of A2 column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-p",
        default=None,
        type=str,
        help="Name of p-value column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-frq",
        default=None,
        type=str,
        help="Name of FRQ or MAF column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-signed-sumstats",
        default=None,
        type=str,
        help="Name of signed sumstat column in {M}, comma null value (e.g., Z,0 or OR,1). "
        "NB: case insensitive.".format(M=i),
    )
    parser.add_argument(
        "--"+i+"-info",
        default=None,
        type=str,
        help="Name of INFO column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-info-list",
        default=None,
        type=str,
        help="Comma-separated list of INFO columns in {M}. Will filter on the mean. "
        "NB: case insensitive.".format(M=i),
    )
    parser.add_argument(
        "--"+i+"-nstudy",
        default=None,
        type=str,
        help="Name of NSTUDY column in {M} (if not a name that {N} understands). "
        "NB: case insensitive.".format(N=programName, M=i),
    )
    parser.add_argument(
        "--"+i+"-ignore",
        default=None,
        type=str,
        help="Comma-separated list of column names to ignore in {M}.".format(M=i),
    )


# General filtering thresholds (for both base and target, though might be a good idea to allow
# for file specific filtering in the future)
parser.add_argument("--info-min", default=0.9, type=float, help="Minimum INFO score.")
parser.add_argument("--maf-min", default=0.01, type=float, help="Minimum MAF.")
parser.add_argument("--chisq-max", default=None, type=float, help="Max chi^2.")
# Basically --extract in PRSice
parser.add_argument(
    "--merge-alleles",
    default=None,
    type=str,
    help="Same as --merge, except the file should have three columns: SNP, A1, A2, "
    "and all alleles will be matched to the --merge-alleles file alleles.",
)
# Minimum number of sample required
parser.add_argument(
    "--n-min",
    default=None,
    type=float,
    help="Minimum N (sample size). Default is (90th percentile N) / 2.",
)
# Minimum number of studies required
parser.add_argument(
    "--nstudy-min",
    default=None,
    type=float,
    help="Minimum # of studies. Default is to remove everything below the max, "
    "unless there is an N column, in which case do nothing.",
)
# Size of file read
parser.add_argument("--chunksize", default=5e5, type=int, help="Size of chunk when reading in files")
# Now LDSC specific commands
parser.add_argument(
    "--ref-ld",
    default=None,
    type=str,
    help="Use --ref-ld to tell LDSC which LD Scores to use as the predictors in the LD "
    "Score regression. "
    "LDSC will automatically append .l2.ldscore/.l2.ldscore.gz to the filename prefix.",
)
parser.add_argument(
    "--ref-ld-chr",
    default=None,
    type=str,
    help="Same as --ref-ld, but will automatically concatenate .l2.ldscore files split "
    "across 22 chromosomes. LDSC will automatically append .l2.ldscore/.l2.ldscore.gz "
    "to the filename prefix. If the filename prefix contains the symbol @, LDSC will "
    "replace the @ symbol with chromosome numbers. Otherwise, LDSC will append chromosome "
    "numbers to the end of the filename prefix."
    "Example 1: --ref-ld-chr ld/ will read ld/1.l2.ldscore.gz ... ld/22.l2.ldscore.gz"
    "Example 2: --ref-ld-chr ld/@_kg will read ld/1_kg.l2.ldscore.gz ... ld/22_kg.l2.ldscore.gz",
)
parser.add_argument(
    "--w-ld",
    default=None,
    type=str,
    help="Filename prefix for file with LD Scores with sum r^2 taken over SNPs included "
    "in the regression. LDSC will automatically append .l2.ldscore/.l2.ldscore.gz.",
)
parser.add_argument(
    "--w-ld-chr",
    default=None,
    type=str,
    help="Same as --w-ld, but will read files split into 22 chromosomes in the same "
    "manner as --ref-ld-chr.",
)

# Miscellenious settings
parser.add_argument(
    "--leblanc", default=None, type=float, help="Use leblanc correction instead"
)

parser.add_argument(
    "--a1-inc", default=False, action="store_true", help="A1 is the increasing allele."
)
parser.add_argument(
    "--keep-maf",
    default=False,
    action="store_true",
    help="Keep the MAF column (if one exists).",
)
parser.add_argument(
    "--same",
    default=False,
    action="store_true",
    help="Indicate that the column names in base and target are the same",
)
parser.add_argument(
    "--no-check-alleles",
    default=False,
    action="store_true",
    help="For rg estimation, skip checking whether the alleles match. This check is "
    "redundant for pairs of chisq files generated using munge_sumstats.py and the "
    "same argument to the --merge-alleles flag. Not sure if this is required for"
    "the current program. Keep this in as a safety precaution",
)
parser.add_argument(
    "--invert-anyway",
    default=False,
    action="store_true",
    help="Force LDSC to attempt to invert ill-conditioned matrices.",
)
parser.add_argument(
    "--n-blocks", default=200, type=int, help="Number of block jackknife blocks."
)
parser.add_argument(
    "--two-step",
    default=None,
    type=float,
    help="Test statistic bound for use with the two-step estimator. ",
)
parser.add_argument(
    "--null-intercept",
    default=None,
    type=float,
    help="Value to remove from the intercept ",
)

# The default column names as a hash table
default_cnames = {
    # RS NUMBER
    "SNP": "SNP",
    "MARKERNAME": "SNP",
    "SNPID": "SNP",
    "RS": "SNP",
    "RSID": "SNP",
    "RS_NUMBER": "SNP",
    "RS_NUMBERS": "SNP",
    # NUMBER OF STUDIES
    "NSTUDY": "NSTUDY",
    "N_STUDY": "NSTUDY",
    "NSTUDIES": "NSTUDY",
    "N_STUDIES": "NSTUDY",
    # CHR
    "CHR": "CHR",
    "CHROMOSOME": "CHR",
    "#CHROM": "CHR",
    # BP
    "BP": "BP",
    "LOC": "BP",
    # P-VALUE
    "P": "P",
    "PVALUE": "P",
    "P_VALUE": "P",
    "PVAL": "P",
    "P_VAL": "P",
    "GC_PVALUE": "P",
    # ALLELE 1
    "A1": "A1",
    "ALLELE1": "A1",
    "ALLELE_1": "A1",
    "EFFECT_ALLELE": "A1",
    "REFERENCE_ALLELE": "A1",
    "INC_ALLELE": "A1",
    "EA": "A1",
    # ALLELE 2
    "A2": "A2",
    "ALLELE2": "A2",
    "ALLELE_2": "A2",
    "OTHER_ALLELE": "A2",
    "NON_EFFECT_ALLELE": "A2",
    "DEC_ALLELE": "A2",
    "NEA": "A2",
    # N
    "N": "N",
    "NCASE": "N_CAS",
    "CASES_N": "N_CAS",
    "N_CAS": "N_CAS",
    "N_CASE": "N_CAS",
    "N_CASES": "N_CAS",
    "NCONTROL": "N_CON",
    "CONTROLS_N": "N_CON",
    "N_CON": "N_CON",
    "N_CONTROL": "N_CON",
    "N_CONTROLS": "N_CON",
    "WEIGHT": "N",  # metal does this. possibly risky.
    # SIGNED STATISTICS
    "ZSCORE": "Z",
    "Z-SCORE": "Z",
    "GC_ZSCORE": "Z",
    "Z": "Z",
    "OR": "OR",
    "B": "BETA",
    "BETA": "BETA",
    "LOG_ODDS": "LOG_ODDS",  # TODO: Maybe assign this to BETA?
    "EFFECTS": "BETA",
    "EFFECT": "BETA",
    "SIGNED_SUMSTAT": "SIGNED_SUMSTAT",
    # INFO
    "INFO": "INFO",
    # MAF
    "EAF": "FRQ",
    "FRQ": "FRQ",
    "MAF": "FRQ",
    "FRQ_U": "FRQ",
    "F_U": "FRQ",
}

# Description of each column names
describe_cname = {
    "SNP": "Variant ID (e.g., rs number)",
    "P": "P-value",
    "CHR": "Chromosomal location of the SNP",
    "BP": "Base pair coordinate of the SNP",
    "A1": "Allele 1, interpreted as ref allele for signed sumstat.",
    "A2": "Allele 2, interpreted as non-ref allele for signed sumstat.",
    "N": "Sample size",
    "N_CAS": "Number of cases",
    "N_CON": "Number of controls",
    "Z": "Z-score (0 --> no effect; above 0 --> A1 is trait/risk increasing)",
    "OR": "Odds ratio (1 --> no effect; above 1 --> A1 is risk increasing)",
    "BETA": "[linear/logistic] regression coefficient (0 --> no effect; above 0 --> A1 is trait/risk increasing)",
    "LOG_ODDS": "Log odds ratio (0 --> no effect; above 0 --> A1 is risk increasing)",
    "INFO": "INFO score (imputation quality; higher --> better imputation)",
    "FRQ": "Allele frequency",
    "SIGNED_SUMSTAT": "Directional summary statistic as specified by --signed-sumstats.",
    "NSTUDY": "Number of studies in which the SNP was genotyped.",
}

# indicate columns should be numeric
numeric_cols = [
    "P",
    "N",
    "N_CAS",
    "N_CON",
    "Z",
    "OR",
    "BETA",
    "LOG_ODDS",
    "INFO",
    "FRQ",
    "SIGNED_SUMSTAT",
    "NSTUDY",
]

# indicate the null value for each summary statistics
null_values = {"LOG_ODDS": 0, "BETA": 0, "OR": 1, "Z": 0}


class Logger(object):
    """
    Lightweight logging.
    TODO: replace with logging module

    """
    def __init__(self, fh):
        self.log_fh = open(fh, "w")

    def log(self, msg):
        """
        Print to log file and stdout with a single command.

        """
        print(msg, file=self.log_fh)
        print(msg)


def is_gz(file):
    """
    check if file is gz based on magic number
    """
    with open(file, "rb") as input:
        magic = input.read(1)
        if not magic:
            return False
        elif magic.hex() != "1f":
            return False
        magic = input.read(1)
        if not magic:
            return False
        if magic.hex() != "8b":
            return False
        return True


def is_bz2(file):
    """
    check if file is bz2 based on magic number
    """
    with open(file, "rb") as input:
        magic = input.read(1)
        if not magic:
            return False
        if magic.hex() != "42":
            return False
        magic = input.read(1)
        if not magic:
            return False
        if magic.hex() != "5a":
            return False
        magic = input.read(1)
        if not magic:
            return False
        if magic.hex() != "68":
            return False
        return True


def get_compression(fh):
    """
    Read filename suffixes and figure out whether it is gzipped,bzip2'ed or not compressed
    """
    if is_gz(fh):
        compression = "gzip"
        openfunc = gzip.open
    elif is_bz2(fh):
        compression = "bz2"
        openfunc = bz2.BZ2File
    else:
        openfunc = open
        compression = None
    return openfunc, compression


def show_log(args, log):
    # Code for generating the header of the program when it first run
    defaults = vars(parser.parse_args())
    opts = vars(args)
    non_defaults = [x for x in opts.keys() if opts[x] != defaults[x]]
    header = MASTHEAD
    header += "Call: \n"
    header += "./EraSOR.py \\\n"
    options = [
        "--" + x.replace("_", "-") + " " + str(opts[x]) + " \\" for x in non_defaults
    ]
    header += "\n".join(options).replace("True", "").replace("False", "")
    header = header[0:-1] + "\n"
    log.log(header)


def read_header(fh):
    """Read the first line of a file and returns a list with the column names."""
    (openfunc, compression) = get_compression(fh)
    first = openfunc(fh).readline()
    if not isinstance(first, str):
        first = first.decode("utf8")
    return first.split()


def clean_header(header):
    """
    For cleaning file headers.
    - convert to uppercase
    - replace dashes '-' with underscores '_'
    - replace dots '.' (as in R) with underscores '_'
    - remove newlines ('\n')
    """
    return header.upper().replace("-", "_").replace(".", "_").replace("\n", "")

def parse_flag_cnames(log, args, is_base=True):
    """
    Parse flags that specify how to interpret nonstandard column names.
    flag_cnames is a dict that maps (cleaned) arguments to internal column names
    Return the flag_cnames dict and also the null signed sumstat interpretation
    """
    cname_base_options = [
        [args.base_nstudy, "NSTUDY", "--base-nstudy"],
        [args.base_snp, "SNP", "--base-snp"],
        [args.base_N_col, "N", "--base-N"],
        [args.base_N_cas_col, "N_CAS", "--base-N-cas-col"],
        [args.base_N_con_col, "N_CON", "--base-N-con-col"],
        [args.base_a1, "A1", "--base-a1"],
        [args.base_a2, "A2", "--base-a2"],
        [args.base_p, "P", "--base-P"],
        [args.base_frq, "FRQ", "--base-nstudy"],
        [args.base_info, "INFO", "--base-info"],
    ]
    cname_target_options = [
        [args.target_nstudy, "NSTUDY", "--target-nstudy"],
        [args.target_snp, "SNP", "--target-snp"],
        [args.target_N_col, "N", "--target-N"],
        [args.target_N_cas_col, "N_CAS", "--target-N-cas-col"],
        [args.target_N_con_col, "N_CON", "--target-N-con-col"],
        [args.target_a1, "A1", "--target-a1"],
        [args.target_a2, "A2", "--target-a2"],
        [args.target_p, "P", "--target-P"],
        [args.target_frq, "FRQ", "--target-nstudy"],
        [args.target_info, "INFO", "--target-info"],
    ]
    cname_options = cname_target_options if (not is_base) else cname_base_options
    # This will store any non-standard input. Empty if nothing was provided
    # Output format is the standardized column name plus the column type
    flag_cnames = {clean_header(x[0]): x[1] for x in cname_options if x[0] is not None}
    # For info list, as multiple columns were provided, need to iterate them and expand
    info_list = args.target_info_list
    prefix = "target"
    if is_base:
        info_list = args.base_info_list
        prefix = "base"
    if info_list:
        try:
            flag_cnames.update({clean_header(x): "INFO" for x in info_list.split(",")})
        except ValueError:
            log.log(
                "The argument to --{N}-info-list should be a comma-separated list of column names.".format(
                    N=prefix
                )
            )
            raise
    null_value = None
    signed_sumstats = (
        args.target_signed_sumstats if (not is_base) else args.base_signed_sumstats
    )
    if signed_sumstats:
        try:
            cname, null_value = signed_sumstats.split(",")
            null_value = float(null_value)
            flag_cnames[clean_header(cname)] = "SIGNED_SUMSTAT"
        except:
            raise Exception(
                "The argument to --{N}-signed-sumstats should be column header comma number.".format(
                    N=prefix
                )
            )
    return [flag_cnames, null_value]


def filter_info(info, log, info_min):
    """Remove INFO < args.info_min (default 0.9) and complain about out-of-bounds INFO."""
    if type(info) is pd.Series:  # one INFO column
        jj = ((info > 2.0) | (info < 0)) & info.notnull()
        ii = info >= info_min
    elif type(info) is pd.DataFrame:  # several INFO columns
        jj = ((info > 2.0) & info.notnull()).any(axis=1) | (
            (info < 0) & info.notnull()
        ).any(axis=1)
        ii = info.sum(axis=1) >= info_min * (len(info.columns))
    else:
        raise ValueError("Expected pd.DataFrame or pd.Series.")

    bad_info = jj.sum()
    if bad_info > 0:
        msg = "WARNING: {N} SNPs had INFO outside of [0,2]. The INFO column may be mislabeled."
        log.log(msg.format(N=bad_info))
    return ii


def filter_frq(frq, log, maf_min):
    """
    Filter on MAF. Remove MAF < args.maf_min and out-of-bounds MAF.
    """
    jj = (frq < 0) | (frq > 1)
    bad_frq = jj.sum()
    if bad_frq > 0:
        msg = "WARNING: {N} SNPs had FRQ outside of [0,1]. The FRQ column may be mislabeled."
        log.log(msg.format(N=bad_frq))

    frq = np.minimum(frq, 1 - frq)
    ii = frq > maf_min
    return ii & ~jj


def filter_pvals(P, log):
    """Remove out-of-bounds P-values"""
    ii = (P > 0) & (P <= 1)
    bad_p = (~ii).sum()
    if bad_p > 0:
        msg = (
            "WARNING: {N} SNPs had P outside of (0,1]. The P column may be mislabeled."
        )
        log.log(msg.format(N=bad_p))

    return ii


def filter_alleles(a):
    """Remove alleles that do not describe strand-unambiguous SNPs"""
    return a.isin(VALID_SNPS)


def parse_dat(dat_gen, convert_colname, merge_alleles, log, args, is_base):
    """Parse and filter a sumstats file chunk-wise"""
    sumstats = args.target if (not is_base) else args.base
    tot_snps = 0
    dat_list = []
    msg = "Reading sumstats from {F} into memory {N} SNPs at a time."
    log.log(msg.format(F=sumstats, N=int(args.chunksize)))
    # Count number of drops due to different reasons
    drops = {"NA": 0, "P": 0, "INFO": 0, "FRQ": 0, "A": 0, "SNP": 0, "MERGE": 0}
    for block_num, dat in enumerate(dat_gen):
        sys.stdout.write(".")
        tot_snps += len(dat)
        old = len(dat)
        # Check wrong type before we change the name so that we don't have to 
        # due with situation where there are duplicated column names
        wrong_types = [
            c
            for c in dat.columns
            if c in numeric_cols and not np.issubdtype(dat[c].dtype, np.number)
        ]
        if len(wrong_types) > 0:
            raise ValueError(
                "Columns {} are expected to be numeric".format(wrong_types)
            )
        # Rename column names to the standard names
        # Move it up so that the INFO filter is in effect
        dat.columns = map(lambda x: convert_colname[x], dat.columns)
        dat = dat.dropna(
            axis=0, how="any", subset=filter(lambda x: x != "INFO", dat.columns)
        ).reset_index(drop=True)
        drops["NA"] += old - len(dat)
        # Check data type is correct
        
        # Now count number of correct columns
        ii = np.full(len(dat), True)
        # Remove SNPs not presented in merge_allele file
        if args.merge_alleles:
            old = ii.sum()
            ii = dat.SNP.isin(merge_alleles.SNP)
            drops["MERGE"] += old - ii.sum()
            # no SNP left, read another chunck
            if ii.sum() == 0:
                continue
            dat = dat[ii].reset_index(drop=True)
            ii = np.full(len(dat), True)

        if "INFO" in dat.columns:
            old = ii.sum()
            ii &= filter_info(dat["INFO"], log, args.info_min)
            new = ii.sum()
            drops["INFO"] += old - new
            old = new
        if "FRQ" in dat.columns:
            old = ii.sum()
            ii &= filter_frq(dat["FRQ"], log, args.maf_min)
            new = ii.sum()
            drops["FRQ"] += old - new
            old = new

        ii &= filter_pvals(dat.P, log)
        new = ii.sum()
        drops["P"] += old - new
        old = new
        # Change allele to upper case
        dat.A1 = dat.A1.astype(str).str.upper()
        dat.A2 = dat.A2.astype(str).str.upper()
        # Filter by combination of A1 and A2
        # This also mean that we will filter anything that ain't SNP
        ii &= filter_alleles(dat.A1 + dat.A2)
        new = ii.sum()
        # Count number of SNPs filtered due to ambiguous SNPs
        drops["A"] += old - new
        old = new
        if ii.sum() == 0:
            continue
        # Append the post filtered data frame to our result
        dat_list.append(dat[ii].reset_index(drop=True))
    msg = "Read {N} SNPs from summary statistic file.\n".format(N=tot_snps)
    if args.merge_alleles:
        msg += "Removed {N} SNPs not in --merge-alleles.\n".format(N=drops["MERGE"])
    msg += "Removed {N} SNPs with missing values.\n".format(N=drops["NA"])
    msg += "Removed {N} SNPs with INFO <= {I}.\n".format(
        N=drops["INFO"], I=args.info_min
    )
    msg += "Removed {N} SNPs with MAF <= {M}.\n".format(N=drops["FRQ"], M=args.maf_min)
    msg += "Removed {N} SNPs with out-of-bounds p-values.\n".format(N=drops["P"])
    msg += "Removed {N} variants that were not SNPs or were strand-ambiguous.\n".format(
        N=drops["A"]
    )
    if not dat_list:
        msg = "All SNPs removed from data\n" + msg + "\n"
        raise Exception(msg)
    sys.stdout.write(" done\n")
    sumstat_data = pd.concat(dat_list, axis=0).reset_index(drop=True)
    msg += "{N} SNPs remain.".format(N=len(sumstat_data))
    log.log(msg)
    return sumstat_data


def get_cname_map(flag, default, ignore):
    """
    Figure out which column names to use.

    Priority is
    (1) ignore everything in ignore
    (2) use everything in flags that is not in ignore
    (3) use everything in default that is not in ignore or in flags

    The keys of flag are cleaned. The entries of ignore are not cleaned. The keys of defualt
    are cleaned. But all equality is modulo clean_header().

    """
    clean_ignore = [clean_header(x) for x in ignore]
    cname_map = {x: flag[x] for x in flag if x not in clean_ignore}
    # Get type of columns that we have already read
    exists = cname_map.values()
    cname_map.update(
        {
            x: default[x]
            for x in default
            if x not in clean_ignore and x not in flag.keys() and default[x] not in exists
        }
    )
    return cname_map


def get_merge_alleles(args, log):
    if args.merge_alleles:
        log.log(
            "Reading list of SNPs for allele merge from {F}".format(
                F=args.merge_alleles
            )
        )
        (openfunc, compression) = get_compression(args.merge_alleles)
        merge_alleles = pd.read_csv(
            args.merge_alleles,
            compression=compression,
            header=0,
            delim_whitespace=True,
            na_values=".",
        )
        if not {"SNP", "A1", "A2"}.issubset(merge_alleles.columns):
            raise ValueError("--merge-alleles must have columns SNP, A1, A2.")
        log.log("Read {N} SNPs for allele merge.".format(N=len(merge_alleles)))
        merge_alleles["A1"] = merge_alleles.A1.astype(str).str.upper()
        merge_alleles["A2"] = merge_alleles.A2.astype(str).str.upper()
        merge_alleles.rename(columns={"A1": "extract1"}, inplace=True)
        merge_alleles.rename(columns={"A2": "extract2"}, inplace=True)
        # Retain only SNP, extract1 and extract2
        merge_alleles = merge_alleles[["SNP", "extract1", "extract2"]]
    else:
        merge_alleles = None
    return merge_alleles


def process_n(dat, n_min, nstudy_min, user_n, user_ncase, user_ncon, log):
    """Determine sample size from --N* flags or N* columns. Filter out low N SNPs."""
    # TODO: Do we want to honor --base-N and --base-N-CAS when we found the column within the file?
    if {"N_CAS", "N_CON"}.issubset(dat.columns):
        N = dat.N_CAS + dat.N_CON
        P = dat.N_CAS / N
        dat["N"] = N * P / P[N == N.max()].mean()
        dat.drop(["N_CAS", "N_CON"], inplace=True, axis=1)
        # NB no filtering on N done here -- that is done in the next code block

    if "N" in dat.columns:
        n_min = n_min if n_min else dat.N.quantile(0.9) / 1.5
        old = len(dat)
        dat = dat[dat.N >= n_min].reset_index(drop=True)
        new = len(dat)
        log.log(
            "Removed {M} SNPs with N < {MIN} ({N} SNPs remain).".format(
                M=old - new, N=new, MIN=n_min
            )
        )

    elif "NSTUDY" in dat.columns:
        nstudy_min = nstudy_min if nstudy_min else dat.NSTUDY.max()
        old = len(dat)
        dat = (
            dat[dat.NSTUDY >= nstudy_min]
            .drop(["NSTUDY"], axis=1)
            .reset_index(drop=True)
        )
        new = len(dat)
        log.log(
            "Removed {M} SNPs with NSTUDY < {MIN} ({N} SNPs remain).".format(
                M=old - new, N=new, MIN=nstudy_min
            )
        )

    if "N" not in dat.columns:
        if user_n:
            dat["N"] = user_n
            log.log("Using N = {N}".format(N=user_n))
        elif user_ncase and user_ncon:
            dat["N"] = user_ncase + user_ncon
            msg = "Using N_cas = {N1}; N_con = {N2}"
            log.log(msg.format(N1=user_ncase, N2=user_ncon))
        else:
            raise ValueError(
                "Cannot determine N. This message indicates a bug.\n"
                "N should have been checked earlier in the program."
            )

    return dat


def p_to_z(P):
    """Convert P-value to standardized beta."""
    return np.sqrt(chi2.isf(P, 1))


def check_median(x, expected_median, tolerance, name):
    """Check that median(x) is within tolerance of expected_median."""
    m = np.median(x)
    if np.abs(m - expected_median) > tolerance:
        msg = "WARNING: median value of {F} is {V} (should be close to {M}). This column may be mislabeled."
        #raise ValueError(msg.format(F=name, M=expected_median, V=round(m, 2)))
    else:
        msg = "Median value of {F} was {C}, which seems sensible.".format(C=m, F=name)

    return msg


def allele_merge(dat, alleles, log):
    """
    WARNING: dat now contains a bunch of NA's~
    Note: dat now has the same SNPs in the same order as --merge alleles.
    """
    dat = pd.merge(alleles, dat, how="left", on="SNP", sort=False).reset_index(
        drop=True
    )
    ii = dat.A1.notnull()
    a1234 = dat.A1[ii] + dat.A2[ii] + dat.extract1[ii] + dat.extract2[ii]
    match = a1234.apply(lambda y: y in MATCH_ALLELES)
    jj = pd.Series(np.zeros(len(dat), dtype=bool))
    jj[ii] = match
    old = ii.sum()
    n_mismatch = (~match).sum()
    if n_mismatch < old:
        log.log(
            "Removed {M} SNPs whose alleles did not match --merge-alleles "
            "({N} SNPs remain).".format(M=n_mismatch, N=old - n_mismatch)
        )
    else:
        raise ValueError("All SNPs have alleles that do not match --merge-alleles.")
    dat = dat.dropna(axis=0, how="any")
    # dat.loc[~jj.astype(bool), [i for i in dat.columns if i != 'SNP']] = float('nan')
    dat.drop({"extract1", "extract2"}, axis=1, inplace=True)
    return dat


def munge_sumstats(merge_alleles, args, log, is_base=True):
    # Read in the header of the file
    sumstats = args.target
    if is_base:
        sumstats = args.base
    try:
        file_cnames = read_header(sumstats)
        # signed_sumstat_null will be a single number
        flag_cnames, signed_sumstat_null = parse_flag_cnames(log, args, is_base)
        # ignore the following columns
        ignore_input = args.target_ignore if (not is_base) else args.base_ignore
        if ignore_input:
            ignore_input = ignore_input.split(",")
            ignore_cnames = [clean_header(x) for x in ignore_input]
        else:
            ignore_cnames = []
        # if signed sumstats or a1-inc is used,
        # remove LOG_ODDS, BETA, Z, OR from the default list
        signed_col = (
            args.target_signed_sumstats if (not is_base) else args.base_signed_sumstats
        )
        
        if signed_col is not None or args.a1_inc:
            mod_default_cnames = {
                x: default_cnames[x]
                for x in default_cnames
                if default_cnames[x] not in null_values
            }
        else:
            mod_default_cnames = default_cnames
        cname_map = get_cname_map(flag_cnames, mod_default_cnames, ignore_cnames)
        # Get detected headers and their descriptions
        cname_translation = {
            x: cname_map[clean_header(x)]
            for x in file_cnames
            if clean_header(x) in cname_map
        }  # note keys not cleaned

        # auto detect the variable names
        if signed_col is None and not args.a1_inc:
            sign_cnames = [
                x for x in cname_translation if cname_translation[x] in null_values
            ]
            if len(sign_cnames) > 1:
                raise ValueError(
                    "Too many signed sumstat columns. Specify which to ignore with the --ignore flag."
                )
            if len(sign_cnames) == 0:
                raise ValueError("Could not find a signed summary statistic column.")
            sign_cname = sign_cnames[0]
            signed_sumstat_null = null_values[cname_translation[sign_cname]]
            cname_translation[sign_cname] = "SIGNED_SUMSTAT"
        else:
            sign_cname = "SIGNED_SUMSTATS"
        # check that we have all the columns we need
        if not args.a1_inc:
            req_cols = ["SNP", "P", "SIGNED_SUMSTAT"]
        else:
            req_cols = ["SNP", "P"]
        for c in req_cols:
            if c not in cname_translation.values():
                raise ValueError("Could not find {C} column.".format(C=c))
        # Check for duplicated column names
        for field in cname_translation:
            numk = file_cnames.count(field)
            if numk > 1:
                raise ValueError(
                    "Found {num} columns named {C}".format(C=field, num=str(numk))
                )
        for head in cname_translation.values():
            numc = list(cname_translation.values()).count(head)
            if numc > 1 and head != "INFO":
                raise ValueError(
                    "Found {num} different {C} columns".format(C=head, num=str(numc))
                )

        n_info = args.target_N if (not is_base) else args.base_N
        n_case_info = args.target_N_cas if (not is_base) else args.base_N_cas
        n_control_info = args.target_N_con if (not is_base) else args.base_N_con

        if (
            (not n_info)
            and (not (n_case_info and n_control_info))
            and ("N" not in cname_translation.values())
            and (not {"N_CAS", "N_CON"}.issubset(cname_translation.values()))
        ):
            raise ValueError("Could not determine N.")
        if (
            "N" in cname_translation.values()
            or {"N_CAS", "N_CON"}.issubset(cname_translation.values())
        ) and "NSTUDY" in cname_translation.values():
            nstudy = [x for x in cname_translation if cname_translation[x] == "NSTUDY"]
            for x in nstudy:
                del cname_translation[x]
        if not {"A1", "A2"}.issubset(cname_translation.values()):
            raise ValueError("Could not find A1/A2 columns.")
        # Only read in the description after we have detected all the column names
        cname_description = {
            x: describe_cname[cname_translation[x]] for x in cname_translation
        }
        log.log("Interpreting column names as follows:")
        log.log(
            "\n".join([x + ":\t" + cname_description[x] for x in cname_description])
            + "\n"
        )
        # Check if the summary statistic file is compressed and use the appropriate function to open it
        (openfunc, compression) = get_compression(sumstats)
        # Numeric columns that are included
        num_sumstat_cols = [ k for k, v in cname_translation.items() if v in numeric_cols]
        # Read in the summary statistic file
        dat_gen = pd.read_csv(
            sumstats,
            delim_whitespace=True,
            header=0,
            compression=compression,
            usecols=cname_translation.keys(),
            na_values=[".", "NA"],
            iterator=True,
            chunksize=args.chunksize,
            dtype={c: np.float64 for c in num_sumstat_cols},
        )
        # Read in file
        dat = parse_dat(dat_gen, cname_translation, merge_alleles, log, args, is_base)
        if len(dat) == 0:
            raise ValueError("After applying filters, no SNPs remain.")
        # Remove duplicated SNPs, this will retain one copy
        old = len(dat)
        dat = dat.drop_duplicates(subset="SNP").reset_index(drop=True)
        new = len(dat)
        log.log(
            "Removed {M} SNPs with duplicated rs numbers ({N} SNPs remain).".format(
                M = old - new, N = new
            )
        )
        # filtering on N cannot be done chunkwise
        dat = process_n(
            dat, args.n_min, args.nstudy_min, n_info, n_case_info, n_control_info, log
        )
        dat.P = p_to_z(dat.P)
        dat.rename(columns={"P": "Z"}, inplace=True)
        if not args.a1_inc:
            log.log(
                check_median(dat.SIGNED_SUMSTAT, signed_sumstat_null, 0.1, sign_cname)
            )
            # Give the Z score the sign
            dat.Z *= (-1) ** (dat.SIGNED_SUMSTAT < signed_sumstat_null)
            dat.drop("SIGNED_SUMSTAT", inplace=True, axis=1)
        # do this last so we don't have to worry about NA values in the rest of
        # the program
        if args.merge_alleles:
            dat = allele_merge(dat, merge_alleles, log)
        log.log("\nMetadata:")
        CHISQ = dat.Z ** 2
        mean_chisq = CHISQ.mean()
        log.log("Mean chi^2 = " + str(round(mean_chisq, 3)))
        if mean_chisq < 1.02:
            log.log("WARNING: mean chi^2 may be too small.")

        log.log("Lambda GC = " + str(round(CHISQ.median() / 0.4549, 3)))
        log.log("Max chi^2 = " + str(round(CHISQ.max(), 3)))
        log.log(
            "{N} Genome-wide significant SNPs (some may have been removed by filtering).".format(
                N=(CHISQ > 29).sum()
            )
        )
        log.log("\nConversion finished")
        return dat
    except Exception:
        log.log("\nERROR converting summary statistics:\n")
        ex_type, ex, tb = sys.exc_info()
        # log.log(traceback.format_exc(ex))
        raise


def _splitp(fstr):
    flist = fstr.split(",")
    flist = [os.path.expanduser(os.path.expandvars(x)) for x in flist]
    return flist


def _read_chr_split_files(chr_arg, not_chr_arg, log, noun, parsefunc, **kwargs):
    """Read files split across 22 chromosomes (annot, ref_ld, w_ld)."""
    try:
        if not_chr_arg:
            log.log("Reading {N} from {F} ...".format(F=not_chr_arg, N=noun))
            out = parsefunc(_splitp(not_chr_arg), **kwargs)
        elif chr_arg:
            f = ps.sub_chr(chr_arg, "[1-22]")
            log.log("Reading {N} from {F} ...".format(F=f, N=noun))
            out = parsefunc(_splitp(chr_arg), _N_CHR, **kwargs)
    except ValueError as e:
        log.log("Error parsing {N}.".format(N=noun))
        raise e

    return out


def _read_M(args, log, n_annot):
    """
    Read M, we don't allow flexibility of using a M file.
    And as we don't add the --not-M-5-50 flag, we will just
    use false here
    """

    if args.ref_ld:
        M_annot = ps.M_fromlist(_splitp(args.ref_ld), common=(not False))
    elif args.ref_ld_chr:
        M_annot = ps.M_fromlist(_splitp(args.ref_ld_chr), _N_CHR, common=(not False))

    try:
        M_annot = np.array(M_annot).reshape((1, n_annot))
    except ValueError as e:
        raise ValueError(
            "# terms in --M must match # of LD Scores in --ref-ld.\n" + str(e.args)
        )

    return M_annot


def _read_ref_ld(args, log):
    """Read reference LD Scores."""
    ref_ld = _read_chr_split_files(
        args.ref_ld_chr,
        args.ref_ld,
        log,
        "reference panel LD Score",
        ps.ldscore_fromlist,
    )
    log.log("Read reference panel LD Scores for {N} SNPs.".format(N=len(ref_ld)))
    return ref_ld


def _check_variance(log, M_annot, ref_ld):
    """Remove zero-variance LD Scores."""
    ii = ref_ld.iloc[:, 1:].var() == 0  # NB there is a SNP column here
    if ii.all():
        raise ValueError("All LD Scores have zero variance.")
    else:
        log.log("Removing partitioned LD Scores with zero variance.")
        ii_snp = np.array([True] + list(~ii))
        ii_m = np.array(~ii)
        ref_ld = ref_ld.iloc[:, ii_snp]
        M_annot = M_annot[:, ii_m]

    return M_annot, ref_ld, ii


def _read_w_ld(args, log):
    """Read regression SNP LD."""
    if (args.w_ld and "," in args.w_ld) or (args.w_ld_chr and "," in args.w_ld_chr):
        raise ValueError("--w-ld must point to a single fileset (no commas allowed).")
    w_ld = _read_chr_split_files(
        args.w_ld_chr, args.w_ld, log, "regression weight LD Score", ps.ldscore_fromlist
    )
    if len(w_ld.columns) != 2:
        raise ValueError("--w-ld may only have one LD Score column.")
    w_ld.columns = ["SNP", "LD_weights"]  # prevent colname conflicts w/ ref ld
    log.log("Read regression weight LD Scores for {N} SNPs.".format(N=len(w_ld)))
    return w_ld


def smart_merge(x, y):
    """Check if SNP columns are equal. If so, save time by using concat instead of merge."""
    if len(x) == len(y) and (x.index == y.index).all() and (x.SNP == y.SNP).all():
        x = x.reset_index(drop=True)
        y = y.reset_index(drop=True).drop("SNP", 1)
        out = pd.concat([x, y], axis=1)
    else:
        out = pd.merge(x, y, how="inner", on="SNP")
    return out


def _merge_and_log(ld, sumstats, noun, log):
    """Wrap smart merge with log messages about # of SNPs."""
    sumstats = smart_merge(ld, sumstats)
    msg = "After merging with {F}, {N} SNPs remain."
    if len(sumstats) == 0:
        raise ValueError(msg.format(N=len(sumstats), F=noun))
    else:
        log.log(msg.format(N=len(sumstats), F=noun))

    return sumstats


def _read_ld_sumstats(args, sumstats, log):
    ref_ld = _read_ref_ld(args, log)
    n_annot = len(ref_ld.columns) - 1
    M_annot = _read_M(args, log, n_annot)
    M_annot, ref_ld, novar_cols = _check_variance(log, M_annot, ref_ld)
    w_ld = _read_w_ld(args, log)
    sumstats = _merge_and_log(ref_ld, sumstats, "reference panel LD", log)
    sumstats = _merge_and_log(sumstats, w_ld, "regression SNP LD", log)
    w_ld_cname = sumstats.columns[-1]
    ref_ld_cnames = ref_ld.columns[1 : len(ref_ld.columns)]
    return M_annot, w_ld_cname, ref_ld_cnames, sumstats, novar_cols


def _merge_sumstats_sumstats(sumstats1, sumstats2, log):
    """Merge two sets of summary statistics."""
    sumstats1.rename(columns={"N": "N1", "Z": "Z1"}, inplace=True)
    sumstats2.rename(
        columns={
            "A1": "A1x",
            "A2": "A2x",
            "N": "N2",
            "Z": "Z2",
            "CHR": "CHR_x",
            "BP": "BP_x",
        },
        inplace=True,
    )
    x = _merge_and_log(sumstats1, sumstats2, "summary statistics", log)
    return x


def _filter_alleles(alleles):
    """Remove bad variants (mismatched alleles, non-SNPs, strand ambiguous)."""
    ii = alleles.apply(lambda y: y in MATCH_ALLELES)
    return ii


def _select_and_log(x, ii, log, msg):
    """Filter down to rows that are True in ii. Log # of SNPs removed."""
    new_len = ii.sum()
    if new_len == 0:
        raise ValueError(msg.format(N=0))
    else:
        x = x[ii]
        log.log(msg.format(N=new_len))
    return x


def _align_alleles(z, alleles):
    """Align Z1 and Z2 to same choice of ref allele (allowing for strand flip)."""
    try:
        z *= (-1) ** alleles.apply(lambda y: FLIP_ALLELES[y])
    except KeyError as e:
        ex_type, ex, tb = sys.exc_info()
        logger.log(traceback.format_exc(ex))
        msg = "Incompatible alleles in .sumstats files: %s. " % e.args
        msg += "Did you forget to use --merge-alleles with munge_sumstats.py?"
        raise KeyError(msg)
    return z


def _check_ld_condnum(args, log, ref_ld):
    """Check condition number of LD Score matrix."""
    if len(ref_ld.shape) >= 2:
        cond_num = int(np.linalg.cond(ref_ld))
        if cond_num > 100000:
            if args.invert_anyway:
                warn = "WARNING: LD Score matrix condition number is {C}. "
                warn += "Inverting anyway because the --invert-anyway flag is set."
                log.log(warn.format(C=cond_num))
            else:
                warn = "WARNING: LD Score matrix condition number is {C}. "
                warn += "Remove collinear LD Scores. "
                raise ValueError(warn.format(C=cond_num))


def _warn_length(log, sumstats):
    if len(sumstats) < 200000:
        log.log("WARNING: number of SNPs less than 200k; this is almost always bad.")


def _read_other_sumstats(args, log, target, base, ref_ld_cnames):
    loop = _merge_sumstats_sumstats(base, target, log)
    loop = loop.dropna(how="any")
    alleles = loop.A1 + loop.A2 + loop.A1x + loop.A2x
    if not args.no_check_alleles:
        loop = _select_and_log(
            loop, _filter_alleles(alleles), log, "{N} SNPs with valid alleles."
        )
    loop["Z2"] = _align_alleles(loop.Z2, alleles)
    cols = [c for c in loop.columns if c in ["A1x", "A2x", "BP_x", "CHR_x"]]
    loop = loop.drop(cols, axis=1)
    # loop = loop.drop(['A1x', 'A2x', 'BP_x','CHR_x'], axis=1)
    _check_ld_condnum(args, log, loop[ref_ld_cnames])
    _warn_length(log, loop)
    return loop


def _rg(sumstats, args, M_annot, ref_ld_cnames, w_ld_cname):
    """Run the regressions."""
    n_snp = len(sumstats)
    s = lambda x: np.array(x).reshape((n_snp, 1))
    if args.chisq_max is not None:
        ii = sumstats.Z1 ** 2 * sumstats.Z2 ** 2 < args.chisq_max ** 2
        n_snp = np.sum(ii)  # lambdas are late binding, so this works
        sumstats = sumstats[ii]
    n_blocks = min(args.n_blocks, n_snp)
    ref_ld = sumstats[ref_ld_cnames].values
    intercepts = [None, None, None]
    rghat = reg.RG(
        s(sumstats.Z1),
        s(sumstats.Z2),
        ref_ld,
        s(sumstats[w_ld_cname]),
        s(sumstats.N1),
        s(sumstats.N2),
        M_annot,
        intercept_hsq1=intercepts[0],
        intercept_hsq2=intercepts[1],
        intercept_gencov=intercepts[2],
        n_blocks=n_blocks,
        twostep=args.two_step,
    )
    return rghat


def estimate_rg(args, base, target, log):
    """
    :param args: is the parameter list
    :param base: is the base summary statistic data structure
    :param target: is the target summary statistic data structure
    :param log: is the logger
    :return:
    A modified version of LDSC sumstats function. Main difference is that it is tailored
    for base and target and will use the data frame directly without reading from file
    """
    args = copy.deepcopy(args)
    # We want the intercept, that's why we remove all constrain parameters
    M_annot, w_ld_cname, ref_ld_cnames, base, _ = _read_ld_sumstats(args, base, log)
    RG = []

    try:
        sumstat_merged = _read_other_sumstats(args, log, target, base, ref_ld_cnames)
        rghat = _rg(sumstat_merged, args, M_annot, ref_ld_cnames, w_ld_cname)
        RG.append(rghat)
        return (
            rghat.hsq1.tot,
            rghat.hsq1.tot_se,
            rghat.hsq1.intercept,
            rghat.hsq2.tot,
            rghat.hsq2.tot_se,
            rghat.hsq2.intercept,
            rghat.gencov.tot,
            rghat.gencov.tot_se,
            rghat.gencov.intercept,
            sumstat_merged,
        )

    except Exception:  # keep going if phenotype 50/100 causes an error
        ex_type, ex, tb = sys.exc_info()
        log.log(traceback.format_exc(ex) + "\n")
        if len(RG) <= 0:  # if exception raised before appending to RG
            RG.append(None)



def diag_matrix(a):
    return np.diag(np.diag(a))

def complement(allele):
    switcher = {"A": "T", "C": "G", "T": "A", "G": "C"}
    return switcher.get(allele, "INVALID")


def first_pass_sumstat(base, target, log):
    dat = pd.merge(base, target, how="inner", on="SNP")
    valid = dat.loc[
        ((dat.A1_x == dat.A1_y) & (dat.A2_x == dat.A2_y))
        | ((dat.A1_x == dat.A2_y) & (dat.A2_x == dat.A1_y))
        | (
            (dat.A1_x == dat.A1_y.apply(complement))
            & (dat.A2_x == dat.A2_y.apply(complement))
        )
        | (
            (dat.A1_x == dat.A2_y.apply(complement))
            & (dat.A2_x == dat.A1_y.apply(complement))
        ),
        "SNP",
    ]
    base_df = pd.merge(valid, base, how="left", on="SNP")
    target_df = pd.merge(valid, target, how="left", on="SNP")
    msg = "{N} SNPs remain after merging two summary statistic file.\n".format(
        N=len(valid)
    )
    log.log(msg)
    return base_df, target_df

def adjust_z(row, intercept):
    #size_weight= inv(np.array([[math.sqrt(row['N1']), 0], [0, math.sqrt(row['N2'])]]))
    #sigma_matrix = size_weight @ intercept @ size_weight
    #variance = inv(diag_matrix(np.sqrt(sigma_matrix)))
    #v_matrix = variance @ sigma_matrix @ variance
    v_matrix = intercept
    z_matrix = np.array([row['Z1'], row['Z2']])
    chol_prec = z_matrix @ linalg.cholesky(inv(v_matrix)).transpose()
    chol_res = linalg.solve(linalg.cholesky(v_matrix), z_matrix.transpose())
    return (chol_res[0] + chol_prec[0])/2
    

if __name__ == "__main__":
    # The program should run in the following order
    # 1. Generate the sumstat file format as required by LDSC
    # 2. Perform the bivariate LDSC analysis to obtain the intercepts
    # 3. Calculate the correlation matrix
    # 4. Adjust the Z-scores
    parameters = parser.parse_args()
    try:
        if parameters.out is None:
            raise ValueError("--out is required.")
    except Exception as e:
        print("\nError: {str}".format(str=e))
        exit(-1)

    logger = Logger(parameters.out + ".log")
    # Now first, process the base file
    # The main difference from munge of LDSC is that we want to store the info
    # directly into a struct to be used later on
    show_log(parameters, logger)
    try:
        # Check if the required info is provided
        if (parameters.ref_ld is None and parameters.ref_ld_chr is None) or (
            parameters.w_ld is None and parameters.w_ld_chr is None):
            raise ValueError(
                "Require LD score for the analysis. Must use --ref-ld or --ref-ld-chr, and --w-ld or --w-ld-chr"
            )
        if parameters.same:
            parameters.target_snp = parameters.base_snp
            parameters.target_N_col = parameters.base_N_col
            parameters.target_N_cas_col = parameters.base_N_cas_col
            parameters.target_N_con_col = parameters.base_N_con_col
            parameters.target_a1 = parameters.base_a1
            parameters.target_a2 = parameters.base_a2
            parameters.target_p = parameters.base_p
            parameters.target_frq = parameters.base_frq
            parameters.target_signed_sumstats = parameters.base_signed_sumstats
            parameters.target_info = parameters.base_info
            parameters.target_info_list = parameters.base_info_list
            parameters.target_nstudy = parameters.base_nstudy
            parameters.target_ignore = parameters.base_ignore
        if parameters.ref_ld and parameters.ref_ld_chr:
            raise ValueError("Cannot set both --ref-ld and --ref-ld-chr.")
        if parameters.w_ld and parameters.w_ld_chr:
            raise ValueError("Cannot set both --w-ld and --w-ld-chr.")
    except Exception as e:
        print("\nError: {str}".format(str=e))
        exit(-1)
    try:
        extract_alleles = get_merge_alleles(parameters, logger)
        print("Processing base summary statistic file")
        base_df = munge_sumstats(extract_alleles, parameters, logger)
        print("\nProcessing target summary statistic file")
        target_df = munge_sumstats(extract_alleles, parameters, logger, False)
        base_df, target_df = first_pass_sumstat(base_df, target_df, logger)
        # We now have the summary statistic information for both the base and target file
        # We can start to perform bivariate LDSC
        print("\nStart running bivariate LDSC")
        (
            base_h2,
            base_h2_se,
            base_inter,
            target_h2,
            target_h2_se,
            target_inter,
            gc,
            gc_se, 
            bi_inter,
            merged_sumstat,
        ) = estimate_rg(parameters, base_df, target_df, logger)
        
        n1 = merged_sumstat["N1"].max()
        n2 = merged_sumstat["N2"].max()
        out = {'base_h2': base_h2, 'base_h2_se': base_h2_se, 'target_h2': target_h2, 'target_h2_se': target_h2_se, 'base_inter': base_inter, 'target_inter': target_inter, 'base_size': n1, 'target_size': n2, 'bi_inter': bi_inter, 'gcov': gc, 'gcov_se': gc_se}
        outLog = pd.DataFrame(data=out, index = [0])
        if parameters.null_intercept:
            bi_inter -= parameters.null_intercept
        # with the exception of the bivariate LD score intercept, intercepts cannot be negative
        if np.isnan(bi_inter):
            bi_inter = 0
        if base_inter < 1:
            base_inter = 1
        if target_inter < 1:
            target_inter
        if parameters.leblanc:
            target_inter = 1
            base_inter = 1
            bi_inter = parameters.leblanc
        else:
            # We don't allow h2 to be less than or equal to 0
            base_adj = 0
            if base_h2 - base_h2_se > 0:
                base_adj = ((base_inter - 1) / (n1 * base_h2))
            target_adj = 0
            if target_h2 - target_h2_se > 0:
                target_adj = ((target_inter - 1) / (n2 * target_h2))
            if abs(gc - gc_se)> 0:
                weighted = ( base_adj * n1 + target_adj * n2) / (n1 + n2)
                bi_inter = bi_inter - gc * weighted * np.sqrt(n1*n2)
            target_inter = 1
            base_inter = 1
            
            
        

        outLog.to_csv(parameters.out + ".meta", index=False)
        # TODO: check whether the origianl matrix is already perfect correlation.
        # If that is the case, and if the sample size of target is smaller than base,
        # we do inverse meta-analysis
        intercepts_matrix = np.array([[base_inter, bi_inter], [bi_inter, target_inter]])
        outLog.to_csv(parameters.out + ".meta", index=False)
        # TODO: check whether the origianl matrix is already perfect correlation.
        # If that is the case, and if the sample size of target is smaller than base,
        # we do inverse meta-analysis
        try:
            #eigen_value, eigen_vector = linalg.eig(v_matrix)
            #adj = eigen_vector @ np.diag(1/np.sqrt(eigen_value)) @ eigen_vector.transpose()
            adjust_z = functools.partial(adjust_z, intercept=intercepts_matrix)
            merged_sumstat["Z"] = merged_sumstat.apply(adjust_z, axis=1)
            print("\nZ-score adjusted, now calculate adjusted P-value")
            merged_sumstat["P"] = np.clip(
                norm.sf(np.absolute(merged_sumstat["Z"])) * 2, 0, 1)
            # Use drop here in case we don't have BP or CHR
            merged_sumstat.drop(
                merged_sumstat.columns.difference(
                    {"CHR", "BP", "SNP", "A1", "A2", "Z", "P", "CP"}
                ),
                axis=1,
                inplace=True,
            )
            merged_sumstat.to_csv(
                path_or_buf=parameters.out + ".assoc.gz",
                sep="\t",
                compression="gzip",
                header=True,
                index=False,
            )
            logger.log("Analysis finished")
        except Exception as e:
            print(e)
            print("Error: Cannot perform Cholesky decomposition on the adjustment matrix. It is possible that the target data is completed nested within the base data. You can perform inversed meta analysis to remove its effect instead. ")
        
    except Exception as e:
        print("\nError: {str}".format(str=e))
